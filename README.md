# ElegantDI
ElegantDI is a dependency-injection container that helps build composite applications.

Here is a list of features implemented
*  Implements DI container with auto-wiring using similar concepts as in MEF.
*  Implemented as a .NET Standard library with no dependencies.
*  Attribute-based class registration using Export and Import attributes. Export attribute defines which classes go into the DI container (called "exports"). Import attribute is used by exported classes to get dependencies from DI container.
*  Field injection is used exclusively.
*  While auto-wiring the dependencies, exports are matched with imports using contracts. A contract is a string (instead of a type) where by default, full type name is taken from the type that is decorated with Export attribute and the type name of a field decorated with Import attribute.
*  Since contract is a string, this allows specifying a custom name for a contract which stays the same in case class is renamed. Custom contract can be specified in Import and Export attributes.
*  A class can have many Export attributes, thereby exporting itself with more than one contract.
*  These are the ways a dependency can be imported:
    *  Simply import it using a class or an interface type.
    *  Lazy import. When a field is of type Lazy<T>, object instantiation is delayed till dependency is used. This improves the application startup speed.
    *  Export factory. When field is of type ExportFactory<T>, the instance of an exported class is created only when calling CreateExport on this class. A new instance is created every time CreateExport is called. This is used to create instances on the fly. Exported class must not be shared in this case. This is usefull for view models and UI controls.
    *  Importing list. When field is of type IEnumerable<T> and ImportMany is used instead of Import attribute, ElegantDI will find all exports matching type T (or contract name from the attribute) and create a list.
    *  Optional exports. By default are all imports required and ElegantDI will not run if any export are missing. Imports can be marked as optional and will be null when export is missing.
*  Catalogs. They store exported classes in the application and therefore act as a central registry. There are many different catalogs serving different purposes. Custom catalog can be created by deriving from Catalog class.
    *  Application catalog: automatically collects all exported classes in the whole application. It scans all DLLs using reflection and no manual registration is required.
    *  Assembly catalog: automatically collects all exported classes from the given assembly.
    *  Type catalog: Adds one or more given types to the catalog.
    *  Aggregate catalog: Aggregates all exports from given sub-catalogs into one larger catalog. This allows creating a tree structure of catalogs.
    *  ExplicitInstance catalog: Registers existing instance of an exported class. This is used when instance of a class already exists. The class used doesn't need an Export attribute.
    *  Filter catalog: Takes a sub-catalog and filters out it's exports based on a filter function. This catalog is used to limit the exports that can be used in the application.
*  CompositeApplication is the main class to use when using ElegantDI.
*  Lifetimes: Shared (default), NonShared, custom lifestyles are supported but may currently conflict with some features.
*  IEntryPoint. When Run method is called on CompositeApplication class, ElegantDI finds a class that exports IEntryPoint interface and calls Run method on it. Only one exported class in the whole catalog can export this interface.
*  Constructor parameter injection. When exported part needs one or more parameters during its construction, parameterized ExportFactory<T,P1,P2,...> must be used. There is no other way of prividing the parameters.
*  ExportType in ExportFactory. When importing a list of export factories, the type of exported class is stored in ExportFactory and can be used to distinguish between exported types. I am not happy with this feature. It may seem useful, it actually brings one big problem. When importing an interface and when importing class has some logic dependant on the type of an imported class (that implements this interface), replacing an implementation of an interface is not possible anymore. This feature will probably be removed!
*  Pre-run time checks. This feature is very important for the integrity of the composition. These checks will be performed before an application is started:
    *  Check that exports for all imports exist. If any required export is missing the application will not start. This prevent run-time errors later on when using the application. Note that when import is optional or when importing a list, this check has no effect.
    *  ExportFactory can only be used on non-shared exports.
    *  Exports have only one private constructor.
    *  Custom checks can also be implemented. An example would be a check that all exported UI controls and view models are not shared.
    *  Check that exports and imports don't form a cyclick dependency. This is not allowed.
    *  When constructor has parameters, check that parameterized ExportFactory is used when importing it and that ExportFactory parameters match the constructor parameters.
*  Measuring the time needed to create and initialize an instance of an export. This information can be used for debugging to determine why an application is slow to start.

There are aditional features built into the library that require a longer description.

<h2>AOP</h2>
When importing an interface, composite application can create a proxy around it (using Castle.DynamicProxy) and give the proxy to the import instead of a real instance. Note that this will only work when importing a dependency using an interface type in the field.

Composite application can then capture all calls made to the proxy which can be used for:
*  Logging/recording method calls (with parameters) into the log file so it can be inspected later for debugging. Check the example implemented as a unit test.
*  Playback. Recorded calls can be later replayed by some tool on the exported class in isolation to determine if it produces the the same output for given input. This can be used to perform automatic unit testing after the class changes where result should stay the same.
*  Security checking. Security check is performed for each method call.
*  Call-path can be determined by tracking the order of called methods.
*  Various other statistics

<h2>Floating exports</h2>
Exported class is normally created only when someone imports it. Sometimes we want to have a class that no one imports (no one depends on this functionality) but still want to create instance of it. I.e. event handler or logging utility attached to some class or used for testing. A class can be marked as "floating" using FloatingExport attribute. Its instance will be created by the ElegantDI itself and keep it alive the whole time. Note that only shared exports can be floating. Without floating export, someone would have to import this class so that ElegantDI would create it. In other words, this someone would reference the functionality that it doesn't need. This violates some design principles.

<h2>Testing</h2>
System-testing with replaced functionality can be performed. This ElegantDI extension library contains ReplacementExport export that must be used with ReplacementTypeCatalog. This catalog must sit on top of all other catalogs. It's function is to replace normal exports with test exports. Tester can take existing application, replace one or more parts with test parts and run it to test different scenarios without the need to change anything in the original system. It can i.e. be used to quickly try out how would UI behave when it needs to display huge amount of data without actually adding this data to the database.

<h2>Security</h2>
Method-call interception can be used to capture calls on interfaces. Each call is checked against an access-control list, if current user has the rights to perform this call. If not, exception is thrown. Check the example implemented as unit test.

<h2>Licensing</h2>
Some exported classes may need a licence in order to be used. Maybe a customer needs to pay for them. Licencing introduces LicensedPart attribute and LicensingCatalog. Catalog must sit on top of all catalogs and it accepts a licence - a list of licenced exports that is usually stored somewhere in a file signed with a certificate. It's job is to filter out exports marked with LicencedPart attribute that are missing in the licence. With this, application can contain only features that the customer bought.

<h2>Simplicity and readability</h2>
ElegantDI was designed to be simple to use and to provide code readability. Therefore the design decisions were:
* Using Export attribute to clearly define what goes into the DI container when a developer opens the code. In DI container that don't use attributes it is hard to see which class should be taken from DI Container and which constructed by hand, because developer must search through the list where classes are registered which can be very long for even a small application.
* Using private field injection and Import attribute clearly show which dependencies a class requires when looking at the code. To me is this much clearer than constructor injection. The problem with this approach is if a class is in an assembly without source code. Then is the reflection the only way to determine the dependencies,
* Exported class must have only one constructor that is private and without parameters (unless parameterized ExportFactory is used). This decision was taken to prevent creating the class from outside the ElegantDI orchestration engine. Only ElegantDI is allowed to create an instance of an export. Having public constructor can lead to bugs where i.e. in the middle of development, a class is changed from manual-instantiation to DI Container instantiation and developer may forget to replace construction with injection. It can also happen that two classes for the same functionality are created in the application which also leads to hard-to-find bugs. ElegantDI will throw an exception if constructor is not private. I know that some will disagree, but ElegantDI's main idea is to orchestrate construction of an application. It is a central composer having control over everything. This leads to better managability and less bugs. Therefore there is no need to do this manually - and should not be allowed. Even in unit tests one can use ElegantDI without any problems. It is not more difficult, in my cases it event results in less code. Manual injection also requires that developer implements it's own Lazy, ExportFactory or any other special functionalities. ElegantDI is simple to use and there is really no reason to do manual injection!
* Can be used for simple and complex applications. Some may say that for small applications there is no point of using a DI Container. I disagree. Initialising ElegantDI requires just three lines (create catalog, create a composite application, call Run). Putting Export attributes on classes and Import on fields is very simple. Later on when application starts to grow, you don't need to switch.

<h2>Final and important note!</h2>
ElegantDI library, like any other DI Container out there, will not make things great and more managable just by itself! I wrote it because it supports me incredibly a lot with building my architectural/design ideas further. Although ElegantDI is very important and groundlaying piece, it is nonetheless only one piece of the software development puzzle. Much more is needed to build quality software. Developer must also make sure to build and organize the application in a proper way (SOLID principles are to start with) and with a good modular design. Only then and in combination with agile mindset, great software can be built that has an evolutionary arhitecture (constantly changing and developing over time) and is agile (higly flexible).

<h2>Examples</h2>
Check the unit tests to see how things work ;).

<h2>Ideas for the future...</h2>
Static code analysis (Roslyn) can be used to search for classes with Import and Export attributes and a dependency map can be built during development. A special VS extension could retrieve the dependencies for the currently-edited class and show in a window which classes use the class currently being edited, and which classes is currently-edited class using. This is practically impossible to do with other DI containers that use fluent registration in code as code would have to be parsed. Dependency errors could be also shown along, like missing dependencies, parameter missmatches in export factories, etc. Custom checks could be added (maybe based on class names), i.e. some class types should always be transient.
