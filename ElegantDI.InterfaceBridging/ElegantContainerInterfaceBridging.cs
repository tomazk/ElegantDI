﻿using Castle.DynamicProxy;
using System;
using System.Reflection;

namespace ElegantDI.InterfaceBridging
{
    public class ElegantContainerInterfaceBridging
    {
        private readonly ElegantContainer container;
        private bool isInterfaceBridgingEnabled = false;
        private ProxyGenerator proxyGenerator;

        public ElegantContainerInterfaceBridging(ElegantContainer container)
        {
            this.container = container;
            proxyGenerator = new ProxyGenerator();

            container.SatisfyingImport += Container_SatisfyingImport;
        }

        private void Container_SatisfyingImport(object sender, SatisfyingImportEventArgs e)
        {
            if (!isInterfaceBridgingEnabled)
                return;
            if (e.Instance == null)
                return;
            // Proceed only if field is an interface.
            if (!e.FieldInfo.FieldType.IsInterface)
                return;
            // Proceed only if current value cannot be assigned to a field
            if (e.FieldInfo.FieldType.IsAssignableFrom(e.Instance.GetType()))
                return;

            // If instance implements the interface with the same name and same methods, a bridge is created. Note that
            // a field's interface can contain a subset of members in the instance interface, but the signatures
            // of matching members must be the same.
            var exportInterfaceType = e.Instance.GetType().GetInterface(e.FieldInfo.FieldType.Name);

            // Skip if the value doesn't implement this interface
            if (exportInterfaceType == null)
                return;

            // Check if member signatures match
            Tools.CheckInterfaceSignatures(e.FieldInfo.FieldType, exportInterfaceType);

            InterfaceBridged?.Invoke(this, new InterfaceBridgedEventArgs(exportInterfaceType, e.FieldInfo.FieldType));

            InterfaceBridgeInterceptor interceptor = new InterfaceBridgeInterceptor(exportInterfaceType, e.Instance);
            object proxy = proxyGenerator.CreateInterfaceProxyWithoutTarget(e.FieldInfo.FieldType, interceptor);
            e.Instance = proxy;
        }

        public void EnableInterfaceBridging()
        {
            isInterfaceBridgingEnabled = true;
        }
        public void DisableInterfaceBridging()
        {
            isInterfaceBridgingEnabled = false;
        }

        public event EventHandler<InterfaceBridgedEventArgs>? InterfaceBridged;
    }

    public class InterfaceBridgedEventArgs : EventArgs
    {
        public Type ExportType { get; }
        public Type ImportType { get; }

        public InterfaceBridgedEventArgs(Type exportType, Type importType)
        {
            this.ExportType = exportType;
            this.ImportType = importType;
        }
    }
}
