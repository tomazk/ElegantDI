﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElegantDI
{
    public class ElegantException : Exception
    {
        public ElegantException(string message)
            : base(message)
        {
        }
    }
}
