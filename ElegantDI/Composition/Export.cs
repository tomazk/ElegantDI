﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ElegantDI.Composition
{
    public class Export
    {
        private Func<IEnumerable<object?>, object> instanceCreator;

        public Export(Func<IEnumerable<object?>, object> instanceCreator, Type exportType)
        {
            this.instanceCreator = instanceCreator;
            this.ExportType = exportType;
        }

        public Type ExportType { get; private set; }

        public object ToInstance()
        {
            return instanceCreator(Array.Empty<object?>());
        }
        public object ToInstance(IEnumerable<object?> constructorAruments)
        {
            return instanceCreator(constructorAruments);
        }
    }
}
