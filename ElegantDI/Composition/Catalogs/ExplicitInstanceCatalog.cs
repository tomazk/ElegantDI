﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ElegantDI.Composition;
using ElegantDI.Composition.LifetimeManagement;

namespace ElegantDI.Composition
{
    public class ExplicitInstanceCatalog : Catalog
    {
        private ComposablePart[] parts;

        public ExplicitInstanceCatalog(params object[] instances)
        {
            parts = instances
                .Select(x => new ComposablePart(x.GetType(), new ExplicitInstanceLifetimeManager(x)))
                .ToArray();
        }
        public ExplicitInstanceCatalog(params ExplicitInstance[] explicitInstances)
        {
            parts = explicitInstances
                .Select(x =>
                {
                    var composabePartInfo = CompositionTools.GetComposablePartInfo(x.Instance.GetType());
                    var exports = new ComposablePartExport[] { new ComposablePartExport(CompositionTools.GetContractFromType(x.ExportedType)) };
                    var imports = composabePartInfo.Imports;

                    return new ComposablePart(x.Instance.GetType(), exports, imports, new ExplicitInstanceLifetimeManager(x.Instance));
                })
                .ToArray();
        }

        public override IEnumerable<ComposablePart> Parts => parts;
    }
    public class ExplicitInstance
    {
        public ExplicitInstance(Type exportedType, object instance)
        {
            // Check that instance is of specified type
            if (!exportedType.IsAssignableFrom(instance.GetType()))
                throw new Exception($"Explicit instance ({instance.GetType().Name}) does not implement the specified type: {exportedType.Name}");

            this.ExportedType = exportedType;
            this.Instance = instance;
        }

        public Type ExportedType { get; private set; }
        public object Instance { get; private set; }
    }
}
