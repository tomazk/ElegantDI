﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ElegantDI.Composition
{
    public enum ImportCardinality { ZeroOrOne, ExactlyOne, ZeroOrMore }
}
