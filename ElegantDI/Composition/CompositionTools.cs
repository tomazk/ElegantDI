﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ElegantDI.Composition
{
    public static class CompositionTools
    {
        /// <summary>
        /// Retrieves contract name from the given type.
        /// </summary>
        public static Contract GetContractFromType(Type type)
        {
            // INFO: IEnumerable<> is not here, because IEnumerable<> itself is a contract type when
            // not importing a list.

            if (type.IsGenericType)
            {
                if (type.GetGenericTypeDefinition().Equals(typeof(ExportFactory<>)))
                    type = type.GenericTypeArguments.Single();
                else if (type.GetGenericTypeDefinition().Equals(typeof(ExportFactory<,>)))
                    type = type.GenericTypeArguments.First();
                else if (type.GetGenericTypeDefinition().Equals(typeof(ExportFactory<,,>)))
                    type = type.GenericTypeArguments.First();
                else if (type.GetGenericTypeDefinition().Equals(typeof(ExportFactory<,,,>)))
                    type = type.GenericTypeArguments.First();
                else if (type.GetGenericTypeDefinition().Equals(typeof(ExportFactory<,,,,>)))
                    type = type.GenericTypeArguments.First();
                else if (type.GetGenericTypeDefinition().Equals(typeof(ExportFactory<,,,,,>)))
                    type = type.GenericTypeArguments.First();
                else if (type.GetGenericTypeDefinition().Equals(typeof(ExportFactory<,,,,,,>)))
                    type = type.GenericTypeArguments.First();
                else if (type.GetGenericTypeDefinition().Equals(typeof(ExportFactory<,,,,,,,>)))
                    type = type.GenericTypeArguments.First();
                else if (type.GetGenericTypeDefinition().Equals(typeof(ExportFactory<,,,,,,,,>)))
                    type = type.GenericTypeArguments.First();
                else if (type.GetGenericTypeDefinition().Equals(typeof(Lazy<>)))
                    type = type.GenericTypeArguments.Single();
            }

            return new Contract(type);
        }

        /// <summary>
        /// Retrieves contract name from the given list type.
        /// </summary>
        public static Contract GetContractFromListType(Type type)
        {
            if (type.IsGenericType &&
                type.GetGenericTypeDefinition().Equals(typeof(IEnumerable<>)))
            {
                return GetContractFromType(type.GenericTypeArguments.Single());
            }
            else
                throw new Exception($"Cannot determine list contract type from '{type.FullName}'.");
        }

        public static ComposabePartInfo GetComposablePartInfo(Type partType)
        {
            var exports = CompositionTools.GetExports(partType, out ExportLifetime lifetime);
            var imports = CompositionTools.GetImports(partType);

            return new ComposabePartInfo(exports, imports, lifetime);
        }

        private static IEnumerable<ComposablePartExport> GetExports(Type partType, out ExportLifetime lifetime)
        {
            //var typeInfo = partType.GetTypeInfo();
            List<ComposablePartExport> list = new List<ComposablePartExport>();
            lifetime = ExportLifetime.Singleton;

            ExportAttribute? attribute = null;

            // Find the attribute and on which type it is defined
            Type attributeOwnerType = partType;

            while (attributeOwnerType != null)
            {
                attribute = attributeOwnerType.GetCustomAttribute<ExportAttribute>(false);
                if (attribute != null)
                    break;

                attributeOwnerType = attributeOwnerType.BaseType;
            }

            if (attribute != null && attributeOwnerType != null)
            {
                // Get exported contracts from attribute
                var contracts = attribute.Contracts.Any() ?
                        attribute.Contracts :
                        new Contract[] { CompositionTools.GetContractFromType(attributeOwnerType) };

                foreach (var contract in contracts)
                {
                    if (!list.Any(x => x.Contract == contract))
                        list.Add(new ComposablePartExport(contract));
                }

                // If there are no contracts in attribute, use the type name as a contract.
                if (!list.Any())
                {
                    Contract contract = CompositionTools.GetContractFromType(attributeOwnerType);
                    list.Add(new ComposablePartExport(contract));
                }

                lifetime = attribute.ExportLifetime;
            }

            return list;
        }
        private static IEnumerable<ComposablePartImport> GetImports(Type partType)
        {
            var typeInfo = partType.GetTypeInfo();
            List<ComposablePartImport> list = new List<ComposablePartImport>();

            Type type = partType;

            while (type != null)
            {
                // Find all fields with Import attribute
                var fields = type.GetFields(BindingFlags.Instance | BindingFlags.NonPublic);

                if (fields != null)
                {
                    foreach (var field in fields)
                    {
                        // Import attribute
                        {
                            var attribute = field.GetCustomAttribute<ImportAttribute>();

                            if (attribute != null)
                                list.Add(new ComposablePartImport(attribute, field));
                        }
                    }
                }

                type = type.BaseType;
            }

            return list;
        }
    }

    public class ComposabePartInfo
    {
        public ComposabePartInfo(IEnumerable<ComposablePartExport> exports, IEnumerable<ComposablePartImport> imports, ExportLifetime lifetime)
        {
            this.Exports = exports;
            this.Imports = imports;
            this.Lifetime = lifetime;
        }

        public IEnumerable<ComposablePartExport> Exports { get; }
        public IEnumerable<ComposablePartImport> Imports { get; }
        public ExportLifetime Lifetime { get; }
    }
}
