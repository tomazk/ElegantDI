﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ElegantDI.Composition
{
    public class ComposablePartExport
    {
        public ComposablePartExport(Contract contract)
        {
            this.Contract = contract;
        }

        public Contract Contract { get; private set; }
    }
}
