﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ElegantDI.Composition
{
    /// <summary>
    /// Represents a contract based on which exports and imports are matched.
    /// </summary>
    public class Contract
    {
        private Type contractType;

        public Type ContractType => contractType;

        public Contract(Type contractType)
        {
            if (contractType == null)
                throw new ArgumentNullException(nameof(contractType));

            this.contractType = contractType;
        }

        public override bool Equals(object obj)
        {
            Contract? other = obj as Contract;

            if (ReferenceEquals(this, other))
                return true;
            else if (ReferenceEquals(other, null))
                return false;

            return this.contractType == other.contractType;
        }
        public override string ToString()
        {
            return contractType.Namespace + "." + contractType.Name;
        }
        public override int GetHashCode()
        {
            return contractType.GetHashCode();
        }

        public static bool operator ==(Contract? contract1, Contract? contract2)
        {
            if (ReferenceEquals(contract1, contract2))
                return true;

            if (ReferenceEquals(contract1, null))
                return false;
            if (ReferenceEquals(contract2, null))
                return false;

            return contract1.contractType == contract2.contractType;
        }
        public static bool operator !=(Contract? contract1, Contract? contract2)
        {
            return !(contract1 == contract2);
        }
    }
}
