﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ElegantDI.Composition
{
    public class TransientLifetimeManager : LifetimeManager
    {
        private Type partType;

        public TransientLifetimeManager(Type partType)
        {
            this.partType = partType;
        }

        public override InstanceInfo GetInstance(IEnumerable<object?> constructorArguments)
        {
            // Always create a new instance. Exported classes must always have max one constructor.
            var constructor = partType.GetConstructors(BindingFlags.Instance | BindingFlags.NonPublic).SingleOrDefault();

            object instance = constructor != null
                ? constructor.Invoke(constructorArguments.ToArray())
                : Activator.CreateInstance(partType);

            return new InstanceInfo(instance, true);
        }
    }
}
