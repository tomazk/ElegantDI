﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using ElegantDI.Composition;

namespace ElegantDI.Features
{
    public class FloatingExportManager
    {
        private ElegantContainer container;
        private List<object> floatingExportInstances = new List<object>();

        public FloatingExportManager(ElegantContainer container)
        {
            this.container = container;

            CreateFloatingInstances();
        }

        private void CreateFloatingInstances()
        {
            // Gather the exports that are marked as floating, create their instances and satisfy their imports.
            // The list is then stored in memory.

            foreach (var part in container.Catalog)
            {
                var floatingInstanceAttribute = part.PartType.GetCustomAttribute<FloatingExportAttribute>();

                if (floatingInstanceAttribute != null)
                {
                    var instance = container.CreateInstance(part, part.PartType, Array.Empty<object?>());
                    floatingExportInstances.Add(instance);
                }
            }
        }
    }
}
