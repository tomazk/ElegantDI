using NUnit.Framework;
using ElegantDI;
using ElegantDI.Composition;
using ElegantDI.Insights;
using System;
using System.Threading;

namespace ElegantDI.Insights.UnitTests
{
    public class PartInitializationTimeMeasurementTests
    {
        private static int sleepTime = 200;
        private double exportAInitialisationTime = 0;
        private bool wasTimeMeasured = false;

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestPartInitializationTimeMeasurement()
        {
            TypeCatalog catalog = new TypeCatalog(
                typeof(ClassA));
            ElegantContainer container = new ElegantContainer(catalog);

            // Init insights
            ElegantContainerInsights insights = new ElegantContainerInsights(container);
            insights.EnablePartInitializationTimeMeasurement();
            insights.PartInitializationTimeMeasured += Insights_PartInitialized;

            Root root = new Root();

            container.Bootstrap(root);
            Assert.IsTrue(wasTimeMeasured);
            // Time measured must be sleepTime +/- some error margin
            Assert.IsTrue(Math.Abs(exportAInitialisationTime - sleepTime) < 30, "Time measured was not within the error margin!");

            // Disable time measurement
            wasTimeMeasured = false;
            exportAInitialisationTime = 0;
            insights.DisablePartInitializationTimeMeasurement();

            Assert.IsTrue(!wasTimeMeasured);
        }

        private void Insights_PartInitialized(object? sender, ElegantDI.Insights.Features.PartInitializedEventArgs e)
        {
            if (e.PartType == typeof(ClassA))
            {
                exportAInitialisationTime = e.TimeInMiliseconds;
                wasTimeMeasured = true;
            }
            else
                throw new Exception("Wrong part type");
        }

        public class Root
        {
            [Import]
            private ClassA? classA = null;

            public ClassA? ClassA => classA;
        }

        #region Classes
        [Export]
        public class ClassA : IPartSatisfiedCallback
        {
            private ClassA()
            {
            }

            void IPartSatisfiedCallback.OnImportsSatisfied()
            {
                Thread.Sleep(sleepTime);
            }
        }
        #endregion
    }
}