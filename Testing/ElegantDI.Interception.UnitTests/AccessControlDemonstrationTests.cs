using NUnit.Framework;
using ElegantDI;
using ElegantDI.Composition;
using ElegantDI.Interception;
using System.Collections.Generic;
using System.Security;

namespace ElegantDI.Interception.UnitTests
{
    public class AccessControlDemonstrationTest
    {
        private AccessControlList accessControlList = new AccessControlList();

        [Test]
        public void TestAccessControlForUser()
        {
            accessControlList.Clear();
            accessControlList.AllowCall(nameof(ApiServer.GetName));

            TypeCatalog catalog = new TypeCatalog(
                typeof(ApiClient),
                typeof(ApiServer));

            ElegantContainer container = new ElegantContainer(catalog);
            ElegantContainerInterceptor interceptor = new ElegantContainerInterceptor(container);
            interceptor.CallIntercepting += Interceptor_CallIntercepted;

            Root root = new Root();

            container.Bootstrap(root);

            if (root.ApiClient == null)
                throw new System.Exception("ApiClient cannot be null");

            string? name = root.ApiClient.GetName();

            // User can get the name
            Assert.AreEqual(name, "Nada");

            // User cannot change the name. SecurityException must be thrown
            SecurityException? se = Assert.Catch(() => root.ApiClient.SetName("Maria")) as SecurityException;
            Assert.NotNull(se);

            // Name must not change
            name = root.ApiClient.GetName();
            Assert.AreEqual(name, "Nada");
        }

        [Test]
        public void TestAccessControlForAdmin()
        {
            accessControlList.Clear();
            accessControlList.AllowCall(nameof(ApiServer.GetName));
            accessControlList.AllowCall(nameof(ApiServer.SetName));

            TypeCatalog catalog = new TypeCatalog(
                typeof(ApiClient),
                typeof(ApiServer));

            ElegantContainer container = new ElegantContainer(catalog);
            ElegantContainerInterceptor interceptor = new ElegantContainerInterceptor(container);
            interceptor.CallIntercepting += Interceptor_CallIntercepted;

            Root root = new Root();
            container.Bootstrap(root);

            string? name = root.ApiClient?.GetName();

            // User can get the name
            Assert.AreEqual(name, "Nada");

            // User cannot change the name. SecurityException must be thrown
            Assert.DoesNotThrow(() => root.ApiClient?.SetName("Maria"));

            // Name must not change
            name = root.ApiClient?.GetName();
            Assert.AreEqual(name, "Maria");
        }

        public class AccessControlList
        {
            List<string> allowedMethods = new List<string>();

            public void AllowCall(string methodName)
            {
                allowedMethods.Add(methodName);
            }
            public void Clear()
            {
                allowedMethods.Clear();
            }
            public void CheckCall(string methodName)
            {
                if (!allowedMethods.Contains(methodName))
                    throw new SecurityException("User is not allowed to call the method " + methodName);
            }
        }

        private void Interceptor_CallIntercepted(object? sender, CallInterceptingEventArgs e)
        {
            accessControlList.CheckCall(e.MemberInvocation.Method.Name);
        }

        class Root
        {
            [Import]
            private ApiClient? apiClient = null;

            public ApiClient? ApiClient => apiClient;
        }

        #region Classes
        [Export]
        public class ApiClient
        {
            [Import]
            private IApiServer? apiServer = null;

            private ApiClient()
            {
            }

            public string? GetName()
            {
                // Simulate a call to server API
                return apiServer?.GetName();
            }
            public void SetName(string name)
            {
                // Simulate a call to server API
                apiServer?.SetName(name);
            }
        }

        public interface IApiServer
        {
            string GetName();
            void SetName(string name);
        }

        [Export(typeof(IApiServer))]
        public class ApiServer : IApiServer
        {
            private string name = "Nada";

            private ApiServer()
            {
            }

            public string GetName()
            {
                return name;
            }
            public void SetName(string name)
            {
                this.name = name;
            }
        }
        #endregion
    }
}