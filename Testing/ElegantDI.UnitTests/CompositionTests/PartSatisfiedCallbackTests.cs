﻿using NUnit.Framework;
using ElegantDI.Composition;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElegantDI.UnitTests.CompositionTests
{
    public class PartSatisfiedCallbackTests
    {
        [Test]
        public void TestPartSatisfiedCallback()
        {
            TypeCatalog catalog = new TypeCatalog(
                   typeof(ExportA));

            ElegantContainer container = new ElegantContainer(catalog);
            Root root = new Root();
            container.Bootstrap(root);

            Assert.AreEqual(root.Item?.PartSatisfied, true);
        }

        class Root
        {
            [Import] private ExportA? item = null;

            public ExportA? Item => item;
        }

        [Export]
        public class ExportA : IPartSatisfiedCallback
        {
            private ExportA()
            {
            }

            public bool PartSatisfied { get; private set; } = false;

            void IPartSatisfiedCallback.OnImportsSatisfied()
            {
                PartSatisfied = true;
            }
        }
    }
}
