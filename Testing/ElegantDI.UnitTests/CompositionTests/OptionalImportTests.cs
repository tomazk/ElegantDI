﻿using NUnit.Framework;
using ElegantDI.Composition;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElegantDI.UnitTests.CompositionTests
{
    public class OptionalImportTests
    {
        [Test]
        public void TestOptionalImport()
        {
            TypeCatalog catalog = new TypeCatalog(
                   typeof(ImportA));

            ElegantContainer container = new ElegantContainer(catalog);
            Root root = new Root();
            container.Bootstrap(root);

            Assert.IsNull(root.Item?.ExportA);
        }

        class Root
        {
            [Import] private ImportA? item = null;

            public ImportA? Item => item;
        }

        #region Classes
        [Export]
        public class ExportA
        {
            private ExportA()
            {
            }
        }
        [Export]
        public class ImportA
        {
            [Import(isOptional: true)]
            private ExportA? exportA = null;

            private ImportA()
            {
            }

            public ExportA? ExportA => exportA;
        }
        #endregion
    }
}
