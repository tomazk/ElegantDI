﻿using NUnit.Framework;
using ElegantDI.Composition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ElegantDI.UnitTests.CompositionTests
{
    public class ImportManyTests
    {
        [Test]
        public void TestImportMany()
        {
            TypeCatalog catalog = new TypeCatalog(
                typeof(ImportA),
                typeof(ExportA),
                typeof(ExportB));

            ElegantContainer container = new ElegantContainer(catalog);
            ImportManyRoot root = new ImportManyRoot();
            container.Bootstrap(root);

            Assert.IsTrue(root.Item?.List.Count() == 2);
        }

        class ImportManyRoot
        {
            [Import]
            private ImportA? item = null;

            public ImportA? Item => item;
        }

        [Test]
        public void TestImportManyLazy()
        {
            TypeCatalog catalog = new TypeCatalog(
                typeof(ImportLazyA),
                typeof(ExportA),
                typeof(ExportB));

            ElegantContainer container = new ElegantContainer(catalog);
            ImportManyLazyRoot root = new ImportManyLazyRoot();
            container.Bootstrap(root);

            Assert.IsTrue(root.Item?.List.Count() == 2);

            ExportBase? export1 = root.Item?.List.First().Value;
            ExportBase? export2 = root.Item?.List.Last().Value;
            Assert.IsTrue((export1 is ExportA && export2 is ExportB) || (export1 is ExportB && export2 is ExportA));
        }

        class ImportManyLazyRoot
        {
            [Import] private ImportLazyA? item = null;

            public ImportLazyA? Item => item;
        }

        [Test]
        public void TestImportManyFactory()
        {
            TypeCatalog catalog = new TypeCatalog(
                typeof(ImportFactoryA),
                typeof(ExportA),
                typeof(ExportB));

            ElegantContainer container = new ElegantContainer(catalog);
            ImportManyFactoryRoot root = new ImportManyFactoryRoot();
            container.Bootstrap(root);

            Assert.IsTrue(root.Item?.List.Count() == 2);

            ExportBase? export1 = root.Item?.List.First().CreateExport();
            ExportBase? export2 = root.Item?.List.Last().CreateExport();
            Assert.IsTrue((export1 is ExportA && export2 is ExportB) || (export1 is ExportB && export2 is ExportA));
        }

        class ImportManyFactoryRoot
        {
            [Import] private ImportFactoryA? item = null;

            public ImportFactoryA? Item => item;
        }

        [Test]
        public void TestImportManyWithTypeFilterFactory()
        {
            TypeCatalog catalog = new TypeCatalog(
                typeof(ImportFactoryA),
                typeof(ExportA),
                typeof(ExportB));

            ElegantContainer container = new ElegantContainer(catalog);
            ImportManyWithTypeFilterRoot root = new ImportManyWithTypeFilterRoot();
            container.Bootstrap(root);

            Assert.IsTrue(root.Item?.List.Count() == 2);

            {
                var exportFactoryA = root.Item?.List.Single(x => x.ExportType == typeof(ExportA));
                ExportBase? exportA = exportFactoryA?.CreateExport();
                Assert.IsNotNull(exportA);
                Assert.AreEqual(exportA?.GetType(), typeof(ExportA));
            }

            {
                var exportFactoryB = root.Item?.List.Single(x => x.ExportType == typeof(ExportB));
                ExportBase? exportB = exportFactoryB?.CreateExport();
                Assert.IsNotNull(exportB);
                Assert.AreEqual(exportB?.GetType(), typeof(ExportB));
            }
        }

        class ImportManyWithTypeFilterRoot
        {
            [Import] private ImportFactoryA? item = null;

            public ImportFactoryA? Item => item;
        }

        #region Classes
        [Export]
        public class ImportA
        {
            [Import]
            private IEnumerable<ExportBase>? list = null;

            private ImportA()
            {
            }

            public IEnumerable<ExportBase>? List => list;
        }
        [Export]
        public class ImportLazyA
        {
            [Import]
            private IEnumerable<Lazy<ExportBase>>? list = null;

            private ImportLazyA()
            {
            }

            public IEnumerable<Lazy<ExportBase>>? List => list;
        }
        [Export]
        public class ImportFactoryA
        {
            [Import]
            private IEnumerable<ExportFactory<ExportBase>>? list = null;

            private ImportFactoryA()
            {
            }

            public IEnumerable<ExportFactory<ExportBase>>? List => list;
        }

        [Export(ExportLifetime.Transient)]
        public abstract class ExportBase
        {
            internal ExportBase()
            {
            }
        }
        public class ExportA : ExportBase
        {
            private ExportA()
            {
            }
        }
        public class ExportB : ExportBase
        {
            private ExportB()
            {
            }
        }
        #endregion
    }
}
