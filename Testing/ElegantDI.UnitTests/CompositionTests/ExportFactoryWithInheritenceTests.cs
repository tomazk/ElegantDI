﻿using NUnit.Framework;
using ElegantDI.Composition;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElegantDI.UnitTests.CompositionTests
{
    public class ExportFactoryWithInheritenceTests
    {
        [Test]
        public void TestThatExportFactoryIsUsedWithConstructorParameters()
        {
            TypeCatalog catalog = new TypeCatalog(
                typeof(ExportA),
                typeof(FactoryImporterWithInheritedExport));

            ElegantContainer container = new ElegantContainer(catalog);
            Root root = new Root();
            container.Bootstrap(root);

            SpecificClass sc = new SpecificClass();

            var import = root.FactoryImporter?.Export?.CreateExport(sc);

            Assert.IsTrue(import?.SomeClass == sc);
        }

        class Root
        {
            [Import]
            private FactoryImporterWithInheritedExport? factoryImporter = null;

            public FactoryImporterWithInheritedExport? FactoryImporter => factoryImporter;
        }

        #region Classes
        public abstract class BaseClass
        {
        }
        public class SpecificClass : BaseClass
        {
        }

        [Export(ExportLifetime.Transient)]
        public class ExportA
        {
            private ExportA(BaseClass someClass)
            {
                this.SomeClass = someClass;
            }

            public BaseClass SomeClass { get; }
        }

        [Export]
        public class FactoryImporterWithInheritedExport
        {
            [Import]
            private ExportFactory<ExportA, SpecificClass>? export = null;

            private FactoryImporterWithInheritedExport()
            {
            }

            public ExportFactory<ExportA, SpecificClass>? Export => export;
        }
        #endregion
    }
}
