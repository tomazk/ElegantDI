﻿using ElegantDI.Composition;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElegantDI.UnitTests.CompositionTests
{
    public class ExportTypeMatchingTest
    {
        [Import]
        private ISomeOtherService someOtherService;

        [Test]
        public void TestExportTypeMatching()
        {
            TypeCatalog catalog = new TypeCatalog(
                typeof(SomeService),
                typeof(SomeOtherService));

            ElegantContainer container = new ElegantContainer(catalog);
            container.Bootstrap(this);
        }

        public interface ISomeService
        {
        }

        [Export(typeof(ISomeService))]
        public class SomeService : ISomeService
        {
            private SomeService()
            {
            }
        }

        public interface ISomeOtherService
        {
        }

        [Export(typeof(ISomeOtherService))]
        public class SomeOtherService : ISomeService // Wrong interface is implemented
        {
            private SomeOtherService()
            {
            }
        }
    }
}
