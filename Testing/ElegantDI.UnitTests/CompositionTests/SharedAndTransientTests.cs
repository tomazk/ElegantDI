﻿using NUnit.Framework;
using ElegantDI.Composition;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElegantDI.UnitTests.CompositionTests
{
    public class SharedAndTransientTests
    {
        [Test]
        public void TestSharedParts()
        {
            TypeCatalog catalog = new TypeCatalog(
                typeof(SharedExport),
                typeof(ImportShared));

            ElegantContainer container = new ElegantContainer(catalog);
            SharedPartsRoot root = new SharedPartsRoot();
            container.Bootstrap(root);

            Assert.IsNotNull(root.Item?.Export1);
            Assert.IsNotNull(root.Item?.Export2);
            Assert.IsTrue(root.Item?.Export1 != null && root.Item?.Export1 == root.Item?.Export2);
        }

        class SharedPartsRoot
        {
            [Import] private ImportShared? item = null;

            public ImportShared? Item => item;
        }

        [Test]
        public void TestTransientParts()
        {
            TypeCatalog catalog = new TypeCatalog(
                typeof(TransientExport),
                typeof(ImportTransient));

            ElegantContainer container = new ElegantContainer(catalog);
            TransientPartsRoot root = new TransientPartsRoot();
            container.Bootstrap(root);


            Assert.IsNotNull(root.Item?.Export1);
            Assert.IsNotNull(root.Item?.Export2);
            Assert.IsTrue(root.Item?.Export1 != null && root.Item?.Export1 != root.Item?.Export2);
        }

        class TransientPartsRoot
        {
            [Import] private ImportTransient? item = null;

            public ImportTransient? Item => item;
        }
    }

    #region Classes
    [Export(ExportLifetime.Transient)]
    public class TransientExport
    {
        private TransientExport()
        {
        }
    }

    [Export]
    public class ImportTransient
    {
        [Import]
        private TransientExport? export1 = null;
        [Import]
        private TransientExport? export2 = null;

        private ImportTransient()
        {
        }

        public TransientExport? Export1 => export1;
        public TransientExport? Export2 => export2;
    }


    [Export]
    public class SharedExport
    {
        private SharedExport()
        {
        }
    }

    [Export]
    public class ImportShared
    {
        [Import]
        private SharedExport? export1 = null;
        [Import]
        private SharedExport? export2 = null;

        private ImportShared()
        {
        }

        public SharedExport? Export1 => export1;
        public SharedExport? Export2 => export2;
    }
    #endregion
}
