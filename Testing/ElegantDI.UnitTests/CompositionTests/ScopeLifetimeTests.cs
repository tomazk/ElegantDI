﻿using NUnit.Framework;
using ElegantDI.Composition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ElegantDI.UnitTests.CompositionTests
{
    public class ScopeLifetimeTests
    {
        [Import]
        private ScopeConsumer? scopeConsumer = null;

        [Test]
        public void TestScopedLifetime()
        {
            TypeCatalog catalog = new TypeCatalog(
                         typeof(Session),
                         typeof(MyScope),
                         typeof(ScopeConsumer),
                         typeof(SessionConsumer));

            ElegantContainer container = new ElegantContainer(catalog);
            Root root = new Root();
            container.Bootstrap(this);

            scopeConsumer?.Process();
        }
    }

    [Export(ExportLifetime.Scoped)]
    public class Session
    {
        public Guid Id { get; } = Guid.NewGuid();

        private Session()
        {
        }

        public void Process()
        {
        }
    }

    [Export]
    public class SessionConsumer
    {
        [Import]
        private Session? session = null;

        private SessionConsumer()
        {
        }

        public void Consume()
        {
            session?.Process();
        }
    }

    [Export(ExportLifetime.Scoped)]
    public class MyScope : ILifetimeScope, IPartSatisfiedCallback
    {
        [Import]
        private Session? session = null;
        [Import]
        private SessionConsumer? sessionConsumer = null;

        private MyScope()
        {
        }

        public void Dispose()
        {
        }

        public void OnImportsSatisfied()
        {
        }

        public void ProcessMessage()
        {
            if (session == null || sessionConsumer == null)
                throw new Exception("Imports cannot be null.");

            session.Process();
            sessionConsumer.Consume();
        }
    }

    [Export]
    public class ScopeConsumer : IPartSatisfiedCallback
    {
        [Import]
        private ExportFactory<MyScope>? myScopefactory = null;

        private ScopeConsumer()
        {
        }

        public void OnImportsSatisfied()
        {
            var myScope = myScopefactory?.CreateExport();
        }

        internal void Process()
        {
            using (var scope = myScopefactory?.CreateExport())
            {
                scope?.ProcessMessage();
            }
        }
    }
}
