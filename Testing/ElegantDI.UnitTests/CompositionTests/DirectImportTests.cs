﻿using NUnit.Framework;
using ElegantDI.Composition;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElegantDI.UnitTests.CompositionTests
{
    public class DirectImportTests
    {
        [Test]
        public void TestImportByTypeContract()
        {
            TypeCatalog catalog = new TypeCatalog(
                typeof(ExportA),
                typeof(ExportB),
                typeof(ExportC),
                typeof(ImportA));

            ElegantContainer container = new ElegantContainer(catalog);

            Root root = new Root();

            container.Bootstrap(root);

            Assert.IsNotNull(root.Import?.ExportA);
            Assert.IsNotNull(root.Import?.ExportB);
            Assert.IsNotNull(root.Import?.ExportC);
        }

        class Root
        {
            [Import]
            private ImportA? import = null;

            public ImportA? Import => import;
        }

        #region Classes
        [Export]
        public class ExportA
        {
            private ExportA()
            {
            }
        }
        [Export(typeof(ExportB))]
        public class ExportB
        {
            private ExportB()
            {
            }
        }
        [Export]
        public class ExportC
        {
            private ExportC()
            {
            }
        }
        [Export]
        public class ImportA
        {
            [Import]
            private ExportA? exportA = null;
            [Import(typeof(ExportB))]
            private ExportB? exportB = null;
            [Import]
            private ExportC? exportC = null;

            private ImportA()
            {
            }

            public ExportA? ExportA => exportA;
            public ExportB? ExportB => exportB;
            public ExportC? ExportC => exportC;
        } 
        #endregion
    }
}
