﻿using NUnit.Framework;
using ElegantDI.Composition;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElegantDI.UnitTests.CompositionTests
{
    public class LazyImportTests
    {
        public static bool LazyExportCreated = false;

        [Test]
        public void TestImportLazy()
        {
            TypeCatalog catalog = new TypeCatalog(
                typeof(ExportA),
                typeof(ImportA));

            ElegantContainer container = new ElegantContainer(catalog);
            Root root = new Root();
            container.Bootstrap(root);

            Assert.AreEqual(LazyImportTests.LazyExportCreated, false);
            ExportA? exportA = root.Item?.ExportA?.Value;
            Assert.AreEqual(LazyImportTests.LazyExportCreated, true);
        }

        class Root
        {
            [Import] private ImportA? item = null;

            public ImportA? Item => item;
        }

        #region Classes
        [Export]
        public class ExportA
        {
            private ExportA()
            {
                LazyImportTests.LazyExportCreated = true;
            }
        }
        [Export]
        public class ImportA
        {
            [Import]
            private Lazy<ExportA>? exportA = null;

            private ImportA()
            {
            }

            public Lazy<ExportA>? ExportA => exportA;
        }
        #endregion
    }
}
