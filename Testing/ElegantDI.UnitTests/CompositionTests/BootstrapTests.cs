﻿using NUnit.Framework;
using ElegantDI.Composition;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElegantDI.UnitTests.CompositionTests
{
    public class BootstrapTests
    {
        [Test]
        public void TestBootstrap()
        {
            ExistingClass instance = new ExistingClass();

            TypeCatalog catalog = new TypeCatalog(
                typeof(ImportA));
            ElegantContainer container = new ElegantContainer(catalog);
            container.Bootstrap(instance);

            Assert.IsNotNull(instance.Import);
        }

        #region Classes
        public class ExistingClass
        {
            [Import]
            private ImportA? import = null;

            public ImportA? Import => import;
        }

        [Export]
        public class ImportA
        {
            private ImportA()
            {
            }
        }
        #endregion
    }
}
