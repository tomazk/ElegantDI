﻿using ElegantDI.Composition;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElegantDI.UnitTests.ProblemCasesTests
{
    public class DuplicateClassRegistrationTests
    {
        [Test]
        public void TestDuplicateClassRegistration()
        {
            TypeCatalog catalog = new TypeCatalog(
                typeof(MainClass),
                typeof(DuplicateClass),
                typeof(DuplicateClass));

            var ex = Assert.Catch(new TestDelegate(() => new ElegantContainer(catalog)));
            Assert.IsNotNull(ex);
        }
    }

    class MainClass
    {
    }

    [Export]
    class DuplicateClass
    {
    }
}
