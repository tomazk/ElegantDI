﻿using NUnit.Framework;
using ElegantDI;
using ElegantDI.Composition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ElegantDI.Tests.CatalogTesting
{
    public class CatalogTests
    {
        [SetUp]
        public void Setup()
        {

        }

        [Test]
        public void TestTypeCatalog()
        {
            Catalog catalog = new TypeCatalog(typeof(ClassA), typeof(ClassB));

            Assert.IsTrue(catalog.Count() == 2);
            Assert.IsTrue(catalog.Any(x => x.PartType == typeof(ClassA)));
            Assert.IsTrue(catalog.Any(x => x.PartType == typeof(ClassB)));
        }

        /*[Test]
        public void TestAssemblyCatalog()
        {
            // TODO
        }

        [Test]
        public void TestApplicationCatalog()
        {
            // TODO
        }*/

        [Test]
        public void TestAggregateCatalog()
        {
            Catalog catalog1 = new TypeCatalog(typeof(ClassA));
            Catalog catalog2 = new TypeCatalog(typeof(ClassB));
            AggregateCatalog catalog = new AggregateCatalog(catalog1, catalog2);

            Assert.IsTrue(catalog.Count() == 2);
            Assert.IsTrue(catalog.Any(x => x.PartType == typeof(ClassA)));
            Assert.IsTrue(catalog.Any(x => x.PartType == typeof(ClassB)));
        }

        [Test]
        public void TestExplicitInstanceCatalog()
        {
            ClassA classAInstance = new ClassA("Class A");
            ClassB classBInstance = new ClassB();

            Catalog catalog = new ExplicitInstanceCatalog(
                new ExplicitInstance(typeof(IClassA), classAInstance),
                new ExplicitInstance(typeof(ClassB), classBInstance));

            ElegantContainer container = new ElegantContainer(catalog);

            Root root = new Root();

            container.Bootstrap(root);

            Assert.IsTrue(classAInstance == root.ClassA);
            Assert.IsTrue(classBInstance == root.ClassB);
        }

        [Test]
        public void TestFiltercatalog()
        {
            Catalog baseCatalog = new TypeCatalog(typeof(ClassA), typeof(ClassB));
            // Filter out ClassB from the catalog
            Catalog catalog = new FilterCatalog(baseCatalog, part => part.PartType != typeof(ClassB));

            Assert.IsTrue(catalog.Count() == 1);
            Assert.IsTrue(catalog.Any(x => x.PartType == typeof(ClassA)));
        }
    }

    class Root
    {
        [Import] private IClassA? classA = null;
        [Import] private ClassB? classB = null;

        public IClassA? ClassA => classA;
        public ClassB? ClassB => classB;
    }

    public interface IClassA
    {
        string Name { get; }
    }
    public class ClassA : IClassA
    {
        private string name;
        public string Name => name;

        public ClassA(string name)
        {
            this.name = name;
        }
    }
    public class ClassB
    {
    }
}
