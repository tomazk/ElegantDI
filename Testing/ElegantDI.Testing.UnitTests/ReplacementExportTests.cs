using NUnit.Framework;
using ElegantDI;
using ElegantDI.Composition;
using ElegantDI.Testing;

namespace ElegantDI.Testing.UnitTests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestReplacementPart()
        {
            TypeCatalog catalog = new TypeCatalog(
                typeof(RealExport),
                typeof(TestClass));
            ReplacementTypeCatalog replacementCatalog = new ReplacementTypeCatalog(catalog,
                typeof(ReplacementExport));

            ElegantContainer container = new ElegantContainer(replacementCatalog);

            Root root = new Root();

            container.Bootstrap(root);
            Assert.IsTrue(root.TestClass?.TestExport is ReplacementExport);
        }

        class Root
        {
            [Import]
            private TestClass? testClass = null;

            public TestClass? TestClass => testClass;
        }

        #region Classes
        interface ITestExport
        {
        }

        [Export(typeof(ITestExport))]
        class RealExport : ITestExport
        {
            private RealExport()
            {
            }
        }
        [ReplacementExport(typeof(ITestExport))]
        class ReplacementExport : ITestExport
        {
            private ReplacementExport()
            {
            }
        }

        [Export]
        class TestClass
        {
            [Import(typeof(ITestExport))]
            private ITestExport? testExport = null;

            private TestClass()
            {
            }

            public ITestExport? TestExport => testExport;
        }
        #endregion
    }
}