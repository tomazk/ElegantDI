﻿using NUnit.Framework;
using ElegantDI.Composition;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElegantDI.InterfaceBridging.UnitTests
{
    public class InterfaceBridgingTests
    {
        [Test]
        public void TestInterfaceBridging()
        {

            TypeCatalog catalog = new TypeCatalog(
                typeof(ClassA),
                typeof(ClassB));
            ElegantContainer container = new ElegantContainer(catalog);
            ElegantContainerInterfaceBridging interfaceBridging = new ElegantContainerInterfaceBridging(container);
            interfaceBridging.EnableInterfaceBridging();

            Root root = new Root();
            container.Bootstrap(root);
            string name = root.ClassA.Test.GetName();
            Assert.AreEqual(name, "Mike");
        }

        class Root
        {
            [Import]
            private ClassA classA = null;

            public ClassA ClassA => classA;
        }

        #region Classes
        // Interface bridging will only work when string is specified as a contract instead of
        // type, because types would otherwise not match.
        [Export]
        public class ClassA
        {
            [Import]
            private ClassA.ITest test = null;

            private ClassA()
            {
            }

            public ClassA.ITest Test => test;

            public interface ITest
            {
                string GetName();
            }
        }

        [Export]
        public class ClassB : ClassB.ITest
        {
            private ClassB()
            {
            }

            public int GetAge()
            {
                return 27;
            }

            public double GetHeight()
            {
                return 1.8;
            }

            public string GetName()
            {
                return "Mike";
            }

            public interface ITest
            {
                string GetName();
                int GetAge();
                double GetHeight();
            }
        }
    
        #endregion
    }
}
