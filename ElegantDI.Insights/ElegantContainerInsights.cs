﻿using ElegantDI.Insights.Features;
using System;

namespace ElegantDI.Insights
{
    public class ElegantContainerInsights
    {
        private ElegantContainer container;

        private PartInitializationTimeMeasurement? moduleLoadTimeMeasurement;

        public ElegantContainerInsights(ElegantContainer container)
        {
            this.container = container;
        }

        public void EnablePartInitializationTimeMeasurement()
        {
            if (moduleLoadTimeMeasurement == null)
            {
                moduleLoadTimeMeasurement = new PartInitializationTimeMeasurement(container);
                moduleLoadTimeMeasurement.PartInitialized += ModuleLoadTimeMeasurement_PartInitialized;
            }
        }

        public void DisablePartInitializationTimeMeasurement()
        {
            if (moduleLoadTimeMeasurement != null)
                moduleLoadTimeMeasurement.Dispose();
        }

        private void ModuleLoadTimeMeasurement_PartInitialized(object sender, PartInitializedEventArgs e)
        {
            PartInitializationTimeMeasured?.Invoke(sender, e);
        }

        public event EventHandler<PartInitializedEventArgs>? PartInitializationTimeMeasured;
    }
}
